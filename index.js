const setup = require('./starter-kit/setup');
const AWS = require('aws-sdk');

exports.handler = async(event, context, callback) => {
    // For keeping the browser launch
    context.callbackWaitsForEmptyEventLoop = false;
    const browser = await setup.getBrowser();
    exports.run(browser).then(
        (result) => callback(null, result)
    ).catch(
        (err) => callback(err)
    );
};

exports.run = async(browser) => {
    const page = await browser.newPage();
    await page.goto('https://google.com', {
        waitUntil: 'networkidle'
    });
    // Type our query into the search bar
    await page.type('puppeteer');

    await page.click('input[type="submit"]');

    // Wait for the results to show up
    await page.waitForSelector('h3 a');

    // const result = await page.evaluate(() => location.href );
    await page.screenshot({
        path: '/tmp/example.png'
    });
    let result = await upload('screenshot.png', '/tmp/example.png');
    await page.close();
    return result;
};

AWS.config.update({
    accessKeyId: 'AKIAITCUKISVFCC537CA',
    secretAccessKey: 'O7jEjuOphothPaGENicwTl0XI/FhUGpfBRvZaoUp'
});
let s3bucket = new AWS.S3({
    params: {
        Bucket: 'stockutils'
    }
});

let fs = require('fs');

function upload(fileName, filePath) {
    return new Promise((accept, reject) => {

        fs.readFile(filePath, function(err, fileData) {
            let params = {
                Key: fileName,
                Body: fileData,
            };

            s3bucket.upload(params, function(err, res) {
                if (err) {
                    accept('Error in uploading file on s3 due to ' + err);
                } else {
                    accept('File successfully uploaded.');
                }
            });
        });
    });
}